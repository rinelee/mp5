package test;

import static org.junit.Assert.*;

import org.junit.Test;
import eece210.marvel.Vertex;

public class VertexTest {
	@Test
	public void createVertex(){
		Vertex rin = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		
		assertEquals(rin.getName(), "Rin Tohsaka");
		assertTrue(rin.getBooks().contains("Fate/Stay Night"));
	}
	
	@Test
	public void addBook(){
		Vertex rin = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		rin.addBook("Fate/Hollow Ataraxia");
		
		assertTrue(rin.getBooks().contains("Fate/Hollow Ataraxia") && rin.getBooks().contains("Fate/Stay Night"));
		assertFalse(rin.getBooks().contains("Fate/Zero"));
	}
	
	@Test
	public void testEqualityTrue(){
		Vertex rin_1 = new Vertex("Tohsaka, Rin", "Fate/Stay Night");
		Vertex rin_2 = new Vertex("Tohsaka, Rin", "Fate/Hollow Ataraxia");
		Object rin_3 = (Object) rin_1;
		
		assertTrue(rin_1.equals(rin_2));
		assertTrue(rin_2.equals(rin_3));
	}
	
	@Test
	public void testEqualityFalse(){
		Vertex rin_1 = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		Vertex rin_2 = new Vertex("test name", "test book");
		
		assertFalse(rin_1.equals(rin_2));
	}

	
}
	