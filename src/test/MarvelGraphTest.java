package test;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;

import org.junit.Test;

import eece210.marvel.MarvelGraph;
import eece210.marvel.Vertex;

public class MarvelGraphTest {
	
	@Test
	public void testCharacterProduction(){
		MarvelGraph typeMoonGraph = null;
		
		try{
		typeMoonGraph = new MarvelGraph("test_edges.txt");
		}
		catch(FileNotFoundException e){
			fail("Should not have thrown exception");
		}
		
		Set<String> characters = typeMoonGraph.getCharacters();
		
		assertTrue(characters.contains("TOHSAKA, RIN") && characters.contains("EMIYA, SHIROU") && characters.contains("MATOU, SAKURA") && characters.contains("ORTENSIA, CAREN"));
	}


	@Test
	public void testBookProduction(){
		MarvelGraph typeMoonGraph = null;
		
		
		try{
			typeMoonGraph = new MarvelGraph("test_edges.txt");
			}
			catch(FileNotFoundException e){
				fail("Should not have thrown exception");
			}
		
		Map<String, Set<Vertex>> graph = typeMoonGraph.getGraph();
		
		
		assertTrue(graph.containsKey("FATE/STAY NIGHT") && graph.containsKey("FATE/HOLLOW ATARAXIA"));
		
		
		Set<Vertex> stayNightChars = graph.get("FATE/STAY NIGHT");
		Set<Vertex> hollowAtaraxiaChars = graph.get("FATE/HOLLOW ATARAXIA");
		
		
		for(Vertex currentChara: hollowAtaraxiaChars){
			assertTrue(currentChara.getName().equalsIgnoreCase("ORTENSIA, CAREN") || currentChara.getName().equalsIgnoreCase("TOHSAKA, RIN") );
		}
		
		for(Vertex currentChara: stayNightChars){
			assertTrue(currentChara.getName().equalsIgnoreCase("MATOU, SAKURA") || currentChara.getName().equalsIgnoreCase("TOHSAKA, RIN") || currentChara.getName().equalsIgnoreCase("EMIYA, SHIROU") );
		}
	
	}
	
	@Test
	public void testMostCentralChar(){
		MarvelGraph typeMoonGraph = null;
		
		try{
		typeMoonGraph = new MarvelGraph("test_edges.txt");
		}
		catch(FileNotFoundException e){
			fail("Should not have thrown exception");
		}
		
		assertEquals(typeMoonGraph.mostCentralCharacter(), "TOHSAKA, RIN"); 
	}
	
	
	@Test
	public void testMarvelFile(){
		MarvelGraph marvelUniverse = null;
		
		try{
		marvelUniverse = new MarvelGraph("labeled_edges.tsv");}
		catch(FileNotFoundException e){
			fail("Should not have thrown exception");
		}
		
		Set<String> characters = marvelUniverse.getCharacters();
		Map<String, Set<Vertex>> graph = marvelUniverse.getGraph();
		
		assertTrue(characters.contains("KRUEGER"));
		assertTrue(graph.containsKey("AA2 35"));
		
	}
	
	

}
