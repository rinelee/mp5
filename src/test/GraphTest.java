package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
	
import org.junit.*;
import eece210.marvel.*;

public class GraphTest {
	
	@Test
	public void testAddDirectVertex(){
		Graph typeMoonUniverse = new Graph();
		Vertex rin = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		
		typeMoonUniverse.addVertex(rin);
		
		assertTrue(typeMoonUniverse.getCharacters().contains("Rin Tohsaka"));
		assertTrue(typeMoonUniverse.getGraph().containsKey("Fate/Stay Night") && typeMoonUniverse.getGraph().get("Fate/Stay Night").contains(rin));
	}
	
	@Test
	public void testIndirectVertex(){
		Graph typeMoonUniverse = new Graph();
		typeMoonUniverse.addVertex("Shirou Emiya", "Fate/Stay Night");
		
		assertTrue(typeMoonUniverse.getCharacters().contains("Shirou Emiya"));
		assertTrue(typeMoonUniverse.getGraph().containsKey("Fate/Stay Night"));
	}
	
	@Test
	public void testDirectPath(){
		Graph typeMoonUniverse = new Graph();
		Vertex Shirou = new Vertex("Shirou Emiya", "Fate/Stay Night");
		Vertex Rin = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		
		typeMoonUniverse.addVertex(Shirou);
		typeMoonUniverse.addVertex(Rin);
		
		ArrayList<Vertex> shortestPath = null;
		
		
		try {
			shortestPath = typeMoonUniverse.findShortestPath("Shirou Emiya", "Rin Tohsaka", 1);
		} catch (Exception e) {
			fail("Should not have thrown exception.");
		}

		
		ArrayList<Vertex> expectedPath = new ArrayList<Vertex>();
		expectedPath.add(Shirou);
		expectedPath.add(Rin);
		
		assertTrue(shortestPath.equals(expectedPath));
		System.out.println();
		
	}
	
	@Test
	public void testIndirectPath(){
		Graph typeMoonUniverse = new Graph();
		Vertex Shirou = new Vertex("Shirou Emiya", "Fate/Stay Night");
		Vertex Rin = new Vertex("Rin Tohsaka", "Fate/Stay Night");
		Vertex Sakura = new Vertex("Sakura Tohsaka", "Fate/Zero");
		Vertex Caren = new Vertex("Caren Ortensia", "Fate/Stay Night");
		
		Rin.addBook("Fate/Zero");
		Caren.addBook("Fate/Zero");
		
		typeMoonUniverse.addVertex(Shirou);
		typeMoonUniverse.addVertex(Rin);
		typeMoonUniverse.addVertex(Sakura);
		typeMoonUniverse.addVertex(Caren);
		
		
		ArrayList<Vertex> shortestPath = null;
		
		
		try {
			shortestPath = typeMoonUniverse.findShortestPath("Shirou Emiya", "Sakura Tohsaka", 10);
		} catch (Exception e) {
			fail("Should not have thrown exception");
		}
		
		ArrayList<Vertex> expectedPath = new ArrayList<Vertex>();
		expectedPath.add(Shirou);
		expectedPath.add(Caren);
		expectedPath.add(Sakura);
		
		assertTrue(shortestPath.equals(expectedPath));
		System.out.println();
		
	}
	
	@Test
	public void testIndirectPath2(){
		Graph typeMoonUniverse = new Graph();
		Vertex Shirou = new Vertex("Shirou Emiya", "Fate/Stay Night");
		Vertex Rin = new Vertex("Rin Tohsaka", "Fate/Stay Night"); 
		Vertex Tokiomi = new Vertex("Tokiomi Tohsaka", "Fate/Zero");
		Vertex Caren = new Vertex("Caren Ortensia", "Fate/Hollow Ataraxia");
		Vertex Caren2 = new Vertex("Caren Ortensia", "Fate/Zero");
		Shirou.addBook("Fate/Hollow Ataraxia");
		Rin.addBook("Fate/Zero");
		Tokiomi.addBook("Fate/Extra");
		Caren.addBook("Fate/Extra");
		
		typeMoonUniverse.addVertex(Shirou);
		typeMoonUniverse.addVertex(Rin);
		typeMoonUniverse.addVertex(Tokiomi);
		typeMoonUniverse.addVertex(Caren);
		typeMoonUniverse.addVertex(Caren2);
		
		
		ArrayList<Vertex> shortestPath = null;
		
		try {
			shortestPath = typeMoonUniverse.findShortestPath("Tokiomi Tohsaka", "Shirou Emiya", 100);
		} catch (Exception e) {
			fail("Should not have found exception.");
		}
		
		ArrayList<Vertex> expectedPath = new ArrayList<Vertex>();
		expectedPath.add(Tokiomi);
		expectedPath.add(Caren);
		expectedPath.add(Shirou);
		
		assertTrue(shortestPath.equals(expectedPath)); //If correct, expected printed output selects Fate/Extra over Fate/Zero
		System.out.println();				  		  // due to lexicographical ordering.
		
		
	}
	
	@Test
	public void testNoPath(){
		Vertex Shirou = new Vertex("Emiya Shirou", "Fate/Stay Night");
		Vertex Akiha = new Vertex("Aozaki Aoko", "Mahoyo");
		
		Graph typeMoonUniverse = new Graph();
		
		typeMoonUniverse.addVertex(Shirou);
		typeMoonUniverse.addVertex(Akiha);
		
		try {
			@SuppressWarnings("unused")
			ArrayList<Vertex> shortestPath = typeMoonUniverse.findShortestPath("Aozaki Aoko", "Emiya Shirou", 2);
			fail("Should have thrown exception.");
		} catch (Exception e) {
			assertEquals(e.getClass(), NoPathException.class);
		}
		
		
	}


	@Test
	public void equalityTest(){
		Graph typeMoonUniverse_1 = new Graph();
		Graph typeMoonUniverse_2 = new Graph();
		Graph typeMoonUniverse_3 = new Graph();
		
		Vertex Shirou = new Vertex("Shirou Emiya", "Fate/Stay Night");
		Vertex Rin = new Vertex("Rin Tohsaka", "Fate/Stay Night"); 
		Vertex Tokiomi = new Vertex("Tokiomi Tohsaka", "Fate/Zero");
		Vertex Caren = new Vertex("Caren Ortensia", "Fate/Hollow Ataraxia");;
		
		Caren.addBook("Fate/Zero");
		Shirou.addBook("Fate/Hollow Ataraxia");
		Rin.addBook("Fate/Zero");
		
		Tokiomi.addBook("Fate/Extra");
		Caren.addBook("Fate/Extra");
		
		typeMoonUniverse_1.addVertex(Shirou);
		typeMoonUniverse_1.addVertex(Rin);
		typeMoonUniverse_1.addVertex(Tokiomi);
		typeMoonUniverse_1.addVertex(Caren);
		
		typeMoonUniverse_2.addVertex(Shirou);
		typeMoonUniverse_2.addVertex(Rin);
		typeMoonUniverse_2.addVertex(Tokiomi);
		typeMoonUniverse_2.addVertex(Caren);
		
		typeMoonUniverse_3.addVertex(Shirou);
		typeMoonUniverse_3.addVertex(Caren);
		
		assertTrue(typeMoonUniverse_1.equals(typeMoonUniverse_2));
		assertFalse(typeMoonUniverse_2.equals(typeMoonUniverse_3));
	}

 

}
