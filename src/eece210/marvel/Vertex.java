package eece210.marvel;

import java.util.*;

/**
 * Class representing a character of the Marvel universe as a vertex on an unweighted graph.
 * 
 * @author Catherine
 *
 */

public class Vertex implements Comparable<Vertex>{
	//Rep invariant:
	//
	//characterName, booksAppeared, edges != null
	//Every Vertex stored in edges also has this Vertex stored in its edges. 
	//The Set<String> 
	//
	//Thread-safety argument:
	//characterName is final and a string with not setter methods. Strings are immutable, so this cannot be affected by threads.
	//Data is only ever added to the booksAppeared and edges, and not removed.
	//Threads are only used for the Breadth First Search, where data in booksAppeared and edges are already produced and
	//only read, not modified.Consequently, this class would be thread safe for our current uses.
	
	private final String characterName;
	private final Set<String> booksAppeared;
	private final TreeMap<Vertex, Set<String>> edges;

	/**
	 * Creates a vertex object representing a character in the Marvel universe.
	 * 		
	 * @requires
	 * 			name and book != null
	 * @param name
	 * 			name of the character represented by the vertex.
	 * @param book
	 * 			the title of a book that the character has appeared in.
	 */
	public Vertex(String name, String book){
		characterName = name;
		
		booksAppeared = new TreeSet<String>();
		booksAppeared.add(book);
		
		edges = new TreeMap<Vertex, Set<String>>();
	}
	
	/**
	 * Adds an additional book to the books this vertex/character belongs to if it has not already been added.
	 * 
	 * @requires 
	 * 			the character actually belongs to the provided book and book != null.
	 * @param book
	 * 			the title of the book to add.
	 * @effect
	 * 			adds the specified book to the books that this character has appeared in if
	 * 			it has not already been added; otherwise does nothing.
	 */
	public void addBook(String book){
		booksAppeared.add(book);
	}
	
	/**
	 * Creates an edge between two characters for a specified book, assuming that the book provided is common
	 * to both characters.
	 * 
	 * @requires
	 * 			vertex and book are not null.
	 * @param vertex
	 * 			the Vertex representing the character to connect to this Vertex.
	 * @param book
	 * 			the book both of these characters appear in.
	 */
	public void addEdge(Vertex vertex, String book){
		Set<String> booklist;
		
		if(!booksAppeared.contains(book)){
			addBook(book); //Add the book if it doesn't already exist.
		}
		
		if(!edges.containsKey(vertex)){
			booklist= new TreeSet<String>(); //Alphabetize the set of strings.
		}
		
		else{
			booklist = edges.get(vertex);
		}
		
		booklist.add(book);
		edges.put(vertex, booklist);
		
	}
	
	/**
	 * Returns the name of the character this vertex represents.
	 * 
	 * @return
	 *		the name of the character represented by this vertex.
	 */
	public String getName(){
		return characterName;
	}
	
	/**
	 * Returns a set of all the books this character has appeared in.
	 * 
	 * @return
	 * 		a set containing all the books that this vertex belongs to, in alphabetical order.
	 */
	public TreeSet<String> getBooks(){
		return new TreeSet<String>(booksAppeared);
	}
	
	/**
	 * Returns the edges of this vertex.
	 * 
	 * @return
	 * 	 a map containing all the edges of this vertex
	 */
	public TreeMap<Vertex, Set<String>> getEdges(){
		return new TreeMap<Vertex, Set<String>>(edges);
	}


/**
 * Compares this vertex to a given vertex. 
 * @param o
 * 			the other vertex to compare this vertex to.
 * @returns
 * 		 0 if the strings are equal, a positive value if this vertex's character is comes before o, alphabetically, negative
 * 		 if the character name comes after o.
 */
	@Override
	public int compareTo(Vertex o) {
		return characterName.compareTo(o.getName());
	}
	
	
	/**
	 * Tests for object equality.
	 * 
	 * @param other
	 * 		the object to test for equality.
	 * @return
	 * 		true if the objects have the same character name, false otherwise.
	 */
	@Override
	public boolean equals(Object other){
		
		if(!(other instanceof Vertex)){
			return false;
		}
		
		return equals((Vertex) other);
	}
	
	
	public boolean equals(Vertex other){
		if(other == null){
			return false;
		}
		
		if(other == this){
			return true;
		}
		
		if(this.hashCode() != other.hashCode()){
			return false;
		}
		
		if(!this.getName().equals(other.getName())){
			return false;
		}
		
		return true; 
	}
	
	
	/**
	 * hashCode for equality testing.
	 */
	@Override
	public int hashCode(){
		return characterName.hashCode();
	}

}
