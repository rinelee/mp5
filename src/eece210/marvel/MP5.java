package eece210.marvel;

import java.io.FileNotFoundException;

public class MP5 {
	public static void main (String []argv){
		MarvelGraph marvelUniverse = null;
	    String fileName = "";
	    String characterA = "";
	    String characterB = "";
	    Integer numThreads = 0;
		
		if(argv.length != 0){
			int i = 0;
			fileName = argv[i];
			characterA = argv[i+1];
			characterB = argv[i+2];
			numThreads = Integer.parseInt(argv[i+3]);
			
		}
		
		//Find the shortest path of between the two inputed characters.
		try {
			marvelUniverse = new MarvelGraph(fileName);
			try {
				marvelUniverse.findShortestPath(characterA, characterB, numThreads);
			} catch (NoPathException e) {
				System.out.println("No path exists!");
			}
		} catch (FileNotFoundException e) {
			System.out.println("File cannot be found!");
		}  
		
		//findMostCentralChar(marvelUniverse); //Uncomment to find the most central character.
	}
	
	/**
	 * 
	 * @param marvelUniverse
	 * 			the graph to find the most central character of.
	 */
	public static void findMostCentralChar(MarvelGraph marvelUniverse){
		System.out.println("The most central character is: " + marvelUniverse.mostCentralCharacter());
	}
	     
}
