package eece210.marvel;

import java.io.*;
import java.util.*;

public class MarvelGraph{
	private Graph marvelUniverse;
	private HashMap<Vertex, String> allVertex;
 	
	/**
	 * Creates a graph using the inputed data information.
	 * 
	 * @param fileName
	 * 			the file to search through.
	 * @throws FileNotFoundException
	 * 			if the inputed file cannot be found.
	 */
	public MarvelGraph(String fileName) throws FileNotFoundException{
		marvelUniverse = new Graph();
		Scanner dataReader = new Scanner(new File(fileName));
		allVertex = new HashMap<Vertex, String>();
		
		while(dataReader.hasNextLine()){
			String line = dataReader.nextLine();
			
			String name = vertexTokenizer(line, true);
			String book = vertexTokenizer(line, false);
			
			Vertex currentVertex = new Vertex(name, book);
			
			marvelUniverse.addVertex(currentVertex);
			allVertex.put(currentVertex, name);	
		}
		
		dataReader.close();
	}
	
	//Reads the file information and retrieves either the name or book title.
	//Param line: The line of text to separate.
	//Param name: True if the name is desired, false if the book title is desired.
	//Requires: line is not null.
	//Returns: the name if name is true, the book title if name is false.
	private String vertexTokenizer(String line, Boolean name){
	
		String[] vertexInfo = line.split("[\\t]");
		String value;
		
		if(name) //If we are seeking name, we want the first value.
		{
			value = vertexInfo[0];
			
		}
		
		else{ //Otherwise, it must be the book we are seeking.
			value = vertexInfo[1];
		}
	
		value = value.substring(1, value.length()-1); //Trim off the quotes.
		return value.trim();
	}

	/**
	 * @return
	 * 		A set that contains the names of all the characters in this graph.
	 */
	public Set<String> getCharacters() {
		return marvelUniverse.getCharacters();
	}

	/**
	 * @return
	 * 		A map containing all the books in the universe as keys matched to the vertices that
	 * 		represent the characters in the book. 
	 */
	public HashMap<String, Set<Vertex>> getGraph() {
		return marvelUniverse.getGraph();
	}

	/**
	 * Determines the shortest path between the two provided characters.
	 * 
	 * @param characterA
	 * 			the name of one of the characters to search for a path between.
	 * @param characterB
	 * 			the name of the other character to search for a path between.
	 * @throws IllegalArgumentException
	 * 			if one or more characters do not exist as a vertex on the graph.
	 * @throws NoPathException 
	 * 				if no path exists between characterA and characterB.
	 * @throws IllegalArgumentException
	 * 				if either characterA or characterB don't exist in the graph.
	 * @return
	 * 			the path containing the shortest distance between the two characters. If
	 * 			multiple shortest paths exist, the alphabetically least path is chosen.
	 */
	public ArrayList<Vertex> findShortestPath(String characterA, String characterB, int numThreads) throws NoPathException {
		return marvelUniverse.findShortestPath(characterA, characterB, numThreads);
	}
	
	
	/**
	 * 
	 * @return
	 * 		The name of the most central character of the universe, that is the vertex on the graph
	 * 		with the shortest path possible between all characters. If no such character exists (the
	 * 		graph is empty or no characters are connected to each other, "N/A" is returned.
	 */
	public String mostCentralCharacter(){
		ArrayList<Thread> threads = new ArrayList<Thread>();
		HashMap<Integer, HashMap<Integer, Vertex>> characterInfo = new HashMap<Integer, HashMap<Integer, Vertex>>();
		
		//Create a new thread for each character.
		for(Vertex character: allVertex.keySet()){
			Thread currentThread = new Thread(new characterSearch(character, characterInfo));
			threads.add(currentThread);
			currentThread.start();
		}
		
		//Ensure all threads complete searching.
		for(Thread thread: threads){
			try {
				thread.join();
			} catch (InterruptedException e) {}
		}
		
		//Identify the character with the most linked characters.
		int highestRelation = determineHighestNonZeroValue(characterInfo.keySet());
	
		if(highestRelation <= 0){ //If there are no relationships between characters in the graph, return "N/A".
			return "N/A";
		}
		
		HashMap<Integer, Vertex> mostRelevant = characterInfo.get(highestRelation);
		int greatestCentrality = determineLowestNonZeroValue(mostRelevant.keySet());
		
		return mostRelevant.get(greatestCentrality).getName();
	}
	
	//Determines the highest non-zero, postive value in a set.
	//Param valuesToSearch: the set to search.
	//Returns the highest positive integer value in the set. 0 if no such value exists.
	private int determineHighestNonZeroValue(Set<Integer> valuesToSearch){
		int highest = 0;
		Queue<Integer> valuesToProcess = new LinkedList<Integer>(valuesToSearch);
		
		while(highest == 0 && !valuesToProcess.isEmpty()){ 
			int currentValue = valuesToProcess.remove();
			if(currentValue  > highest){
				highest = currentValue; //Retrieve first positive value in the queue.
			}
			
		}
	
		for(int currentValue: valuesToProcess){
			if(currentValue > highest){
				highest = currentValue; //Update the highest.
			}
		}

		return highest;
	}
	
	//Determines the lowest non-zero, postive value in a set.
	//Param valuesToSearch: the set to search.
	//Returns the lowest positive integer value in the set.
	private int determineLowestNonZeroValue(Set<Integer> valuesToSearch){
		int lowest = 0;
		Queue<Integer> valuesToProcess = new LinkedList<Integer>(valuesToSearch);
		
		while(lowest == 0 && !valuesToProcess.isEmpty()){ 
			int currentValue = valuesToProcess.remove();
			if(currentValue  > lowest){
				lowest = currentValue; //Retrieve the first positive value in the queue.
			}
		}
	
		for(int currentValue: valuesToProcess){
			if(currentValue > 0 && currentValue < lowest){
				lowest = currentValue; //Replace with lowest positive value.
			}
		}
		
		return lowest;
	}
	
	
	//Thread to search for a specified character.
	//Param character: the character to retrieve information on.
	//Param characterInfo: a map that will store the number of other characters this character is linked to,
	//the sum of this character's path length, and the character, in that order.
	private class characterSearch implements Runnable{
		//Thread safety Argument:
		//Each thread performs an isolated search on threads in relation to other characters.
		//Only characterInfo is shared, but information is simply updated by each Vertex.
		//Even if multiple vertices have the same number of relations, the total path length will
		//differentiate the vertices according to the mapping, so this method is thread-safe.
		Vertex character;
		HashMap<Integer, HashMap<Integer, Vertex>> characterInfo; 
		
		private characterSearch(Vertex character, HashMap<Integer, HashMap<Integer, Vertex>> characterInfo){
			this.character = character;
			this.characterInfo = characterInfo;
		}
		
		public void run(){
			int edgeLengthSum = 0;
			int numFoundPaths = 0;
			
			for(Vertex otherCharacter : allVertex.keySet()){
				if(!otherCharacter.equals(character)){
					try {
						 edgeLengthSum += marvelUniverse.findShortestPath(character.getName(), otherCharacter.getName(), 1).size();
						 numFoundPaths++;
					} catch (NoPathException e) {
						System.out.println("No path between " + character.getName() + " and " + otherCharacter.getName());
					}
				}
			}
			
			HashMap<Integer, Vertex> centralityValue; //Determine the total path length mapped to the character's vertex.
			
			if(characterInfo.containsKey(numFoundPaths)){
				centralityValue = characterInfo.get(numFoundPaths);	
			}
			else{
				centralityValue = new HashMap<Integer, Vertex>(); //Save all the information in shared data, so it can be retrieved later.
			}
			
			centralityValue.put(edgeLengthSum, character);
			characterInfo.put(numFoundPaths, centralityValue);
			
	}
	
	
	
	}
}
