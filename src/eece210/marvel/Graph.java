package eece210.marvel;

import java.util.*;
import java.util.concurrent.*;

/**
 * Class representing a graph of the Marvel universe, composed of characters as vertices and
 * books as edges/paths between the vertices.
 * 
 * @author Catherine Lee, Arabelle Hou.
 *
 */
public class Graph {
	//Rep invariant:
	//universeSummary and allCharacters are not null.
	//All the books that the characters of allCharacters appear in include the character in 
	//its mapped set, and vice-versa.
	//All the characters contained in universeSummary are in allCharacters.
	//All the vertices in allCharacters are mapped to their character name.
	private final HashMap<String, Set<Vertex>> universeSummary; //Stores all books in this graph and the vertices associated with them.
	private final HashMap<String, Vertex> allCharacters; //Lists all characters by name to their respective vertex objects.
	
	/**
	 * Creates a graph representing a universe of characters in books.
	 */
	public Graph(){
		universeSummary = new HashMap<String, Set<Vertex>>(); //Create new data stores for each graph.
		allCharacters = new HashMap<String, Vertex>();
	}
	
	/**
	 * 
	 * Adds a vertex to the graph.
	 * 
	 * @requires
	 * 			vertex is not null and the vertex does not already exist.
	 * @param vertex
	 * 			an existing vertex to be added to the graph.
	 */
	public void addVertex(Vertex vertex){
		if(!allCharacters.containsKey(vertex.getName())){ //Check that the vertex does not already exist in the graph.
			allCharacters.put(vertex.getName(), vertex); //Add in the new vertex.
		}
			
		Vertex storedVertex = allCharacters.get(vertex.getName());
		
		for(String bookName: vertex.getBooks()){ 
			//Then also ensure all the books this character appears in also records this character.
			addBook(storedVertex, bookName); // If the vertex is the same but are different objects with different book values, this is combined.
											
			for(Vertex otherCharacter: universeSummary.get(bookName)){
				storedVertex.addEdge(otherCharacter, bookName);
				otherCharacter.addEdge(storedVertex, bookName);
			}
		}
		
	}
	
	/**
	 * Adds a vertex to the graph.
	 * 
	 * @requires
	 * 			characterName and book are not null.
	 * @param characterName
	 * 			the name of the character of the vertex to add.
	 * @param book
	 * 			the title of the book the character appears in.
	 */
	public void addVertex(String characterName, String book)
	{
		Vertex vertex = new Vertex(characterName, book);
		addVertex(vertex);
	}
	
	//Adds a character to a book to universeSummary.
	//
	//Requires: vertex and book are not null.
	//Param vertex: the vertex associated with the book specified.
	//Param book: the book to add or the book of the vertex to add to universeSummary.
	//Effect: Adds the information provided to universeSummary if it does not already exist.
	//Otherwise does nothing.
	private void addBook(Vertex vertex, String book){
		vertex.addBook(book);
		
		Set<Vertex> characterList;
		
		if(!universeSummary.containsKey(book)){
			characterList= new HashSet<Vertex>();  //Use an empty set if the book does not already exist.
		}
		
		else{
			characterList = universeSummary.get(book); 
		}
		
		characterList.add(vertex);
		universeSummary.put(book, characterList);
		
	}
	
	/**
	 * 
	 * @return
	 * 		A set that contains the names of all the characters in this graph.
	 * 		
	 */
	public Set<String> getCharacters(){
		return allCharacters.keySet();
	}
	
	/**
	 * 
	 * @return
	 * 		A map containing all the books in the universe as keys matched to the vertices that
	 * 		represent the characters in the book. 
	 */
	public HashMap<String, Set<Vertex>> getGraph(){
		return new HashMap<String, Set<Vertex>>(universeSummary); //Although the vertices are directly returned, 
																  //the they are immutable in the sense that 
																  //existing books cannot be removed, so this
																  //isn't a problem.
	}
	
	/**
	 * Determines the shortest path between the two provided characters.
	 * 
	 * @param characterA
	 * 			the name of one of the characters to search for a path between.
	 * @param characterB
	 * 			the name of the other character to search for a path between.
	 * @throws IllegalArgumentException
	 * 			if one or more characters do not exist as a vertex on the graph.
	 * @throws NoPathException 
	 * 				if no path exists between characterA and characterB.
	 * @throws IllegalArgumentException
	 * 				if either characterA or characterB don't exist in the graph.
	 * @return
	 * 			the path containing the shortest distance between the two characters. If
	 * 			multiple shortest paths exist, the alphabetically least path is chosen.
	 */
	public ArrayList<Vertex> findShortestPath(String characterA, String characterB, int numThreads) throws NoPathException{
		//Thread safety argument:
		//Edges of characterA are partitioned into numThreads.
		//Thread safe datatypes are used for retrieving data and the threads will ensure that all possible 
		//search paths are found.
		//Each thread will search using isolated queues.
		//All results are then compared, ensuring that the alphabetically least past will be chosen.
		BlockingQueue<ArrayList<Vertex>> foundResults = new LinkedBlockingQueue<ArrayList<Vertex>>();
		
		//Ensures that characters must be on graph.
		if(!allCharacters.containsKey(characterA) || !allCharacters.containsKey(characterB)){
			throw new IllegalArgumentException("Error: one or more characters(s) specified do not exist.");
		}
		
		//If characters exist, then retrieve them.
		Vertex startingPoint = allCharacters.get(characterA);
		Vertex destination = allCharacters.get(characterB);
		
		BlockingQueue<Vertex> verticesToProcess =  new PriorityBlockingQueue<Vertex>(startingPoint.getEdges().keySet());
		Thread[] searcher = new Thread[numThreads]; //Creates a user-specified number of threads.
		
		for(int index = 0; index < numThreads; index++){
			Thread producer = searcher[index] = new Thread(new Search(destination, verticesToProcess, foundResults));
			producer.start();
		}
		
		for(Thread producer: searcher){
			try {
				producer.join(); //Ensures all threads finish.
			} 
			catch (InterruptedException e) {} 	
		}
		
		if(foundResults.isEmpty()){
			throw new NoPathException(); //If nothing is found, we can conclude that there is no path between the two characters.
		}	
		
		//Determine the shortest sub-path (path of one of the starting point's edges to the target, or target if a direct path.)
		ArrayList<Vertex> shortestSubPath = foundResults.remove();
		
		for(ArrayList<Vertex> currentPath: foundResults){
			shortestSubPath = compareShortestPath(currentPath, shortestSubPath); //Keep only the shortest path, of all the paths found by threads.
		}
		
		
		ArrayList<Vertex> shortestPath = new ArrayList<Vertex>();
		shortestPath.add(startingPoint); //Concatenate the paths, since the search only finds a sub-path.
		shortestPath.addAll(shortestSubPath);
		
		printShortestPath(shortestPath); 
		return shortestPath;
	}
	
	//Thread that retrieves a portion of the edges of the starting vertex and searches 
	//through them for the shortest path amongst that set, similar to partitioning the
	//dataset to be split between each thread.
	private class Search implements Runnable{
		Vertex destination;
		
		BlockingQueue<Vertex> verticesToProcess;
		BlockingQueue<Vertex> workList;
		BlockingQueue<ArrayList<Vertex>> foundResults;
		
		HashMap<Vertex, ArrayList<Vertex>> foundPaths;
		
		private Search(Vertex destination, BlockingQueue<Vertex> verticesToProcess, BlockingQueue<ArrayList<Vertex>> foundResults){
			this.destination = destination;
			this.verticesToProcess = verticesToProcess;
			this.foundResults = foundResults;
		}
		
		public void run() {
			
			workList = new PriorityBlockingQueue<Vertex>(); //Priority Queue ensures lexicographical ordering.
			foundPaths = new HashMap<Vertex, ArrayList<Vertex>>();
			
			while(!verticesToProcess.isEmpty()){
				Vertex currentVertex = verticesToProcess.poll(); //Keep taking from the queue while it has elements.
				
				if(currentVertex != null){
					ArrayList<Vertex> currentPath = new ArrayList<Vertex>(); //Create a path for the Vertex path being formed.
					currentPath.add(currentVertex); 
					foundPaths.put(currentVertex, currentPath);
					workList.offer(currentVertex); //Keep adding items until they are distributed.
				}
					
			}
			
			BFS(); //Perform the breadth-first-search.
			
		}
		
		//Performs breadth-first-search and places the shortest path found in foundResults.
		public void BFS(){
			
			while(!workList.isEmpty()){
				Vertex currentVertex = workList.poll();
				
				if(currentVertex.equals(destination)){ //Check to see if the target has been reached;
					if(foundPaths.get(destination) != null){
						foundResults.offer(foundPaths.get(destination)); //Place the shortest path found by this thread into a retrievable location.
					}
					
					return;
				}
				
				for(Vertex nextVertex: currentVertex.getEdges().keySet()){ 
				//Now search all the vertices adjacent to the current vertex.
				//Alphabetical order is ensured since the edges list is a tree map, and thus ordered.
					
					if(!foundPaths.containsKey(nextVertex)){
						ArrayList<Vertex> currentPath = new ArrayList<Vertex>(foundPaths.get(currentVertex));
						currentPath.add(nextVertex);
						foundPaths.put(nextVertex, currentPath);
						workList.offer(nextVertex);
					}
				}
			}
		}
		
	}
	
	 // Prints the shortest path that was found between two vertices (characters).
	 // @requires shortestPath != null
	 // @param shortestPath
	 //	    the path containing the shortest distance between two characters
	private void printShortestPath(ArrayList<Vertex> shortestPath){
		int shortestPathLength = shortestPath.size();
		
		for(int index = 0; index < shortestPathLength - 1; index++){
			Vertex currentVertex = shortestPath.get(index);
			Vertex nextVertex = shortestPath.get(index+1);
			
			TreeSet<String> currentVertexBooks = currentVertex.getBooks();
			TreeSet<String> nextVertexBooks = nextVertex.getBooks();
			
			for(String currentBook: currentVertexBooks){
						if(nextVertexBooks.contains(currentBook)){ //Since this is a tree set, the first common book will be alphabetically least.
							System.out.println(currentVertex.getName() + " and " + nextVertex.getName() + " are both found in " + currentBook  + ".");
							break;
						}
			}
		}
	}
	
	//  Compares two paths and returns the shortest path in length or, if they are equal size,
	 // returns the alphabetically least path. It will return the first path if 
	 // the two paths are equal alphabetically and in size.
	//
	 // Requires: path1 and path2 != null
	 // Param path1: the first path to compare
	 // Param path2: the second path to compare
	 // Returns: the shorter path if the two paths are different in size, the alphabetically least path if 
	 // the two paths are equal in size, or path1 if the two paths are equal in size and alphabetically.
	   
	private ArrayList<Vertex> compareShortestPath(ArrayList<Vertex> path1, ArrayList<Vertex> path2){
		if(path1.size() < path2.size()){
			return path1;
		}
		else if(path2.size() < path1.size()){
			return path2;
		}
		else{
			for(int i = 0; i < path1.size(); i++){
				int comparison = path1.get(i).getName().compareTo(path2.get(i).getName());
				if(comparison < 0){
					return path1;
				}
				else if(comparison > 0){
					return path2;
				}
			}
		}
		return path1;
	}
	
	/**
	 * Method to check if two Graph objects are equal. 
	 * 
	 * @return	
	 * 		True if the graphs contain all the same characters (vertices) found in the same books; false otherwise.
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		
		if(other == null){
			return false;
		}
		
		if(other.getClass() != Graph.class){
			return false;
		}

		return equals((Graph) other);
	}
	
	public boolean equals(Graph other){
		if(other == this){
			return true;
		}
		
		if(other.hashCode() != hashCode()){
			return false;
		}
		
		if(!other.getGraph().equals(universeSummary)){
			return false;
		}
		
		return true;
	}

	/**
	 * hashCode for testing equality.
	 */
	@Override
	public int hashCode() {
		int hashValue = 0;
		Set<String> vertices = universeSummary.keySet();
		
		for(String currMovie: vertices){
			hashValue += currMovie.hashCode();
		}
		
		return hashValue;
	}

	
}
